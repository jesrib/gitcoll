#!/usr/bin/python3

# helpful for development on Ubuntu 20.04. pip3 install -e . doesn't work, but "setup.py develop --user" does

import setuptools

setuptools.setup()
