#!/usr/bin/python3

import  setuptools
#from  import setup, find_packages


with open("README.md", "rt") as fh:
    readme = fh.read()

with open("LICENSE", "rt") as fh:
    license = fh.read()

setuptools.setup(
    name="gcln",
    version="0.0.1",
    description="Gitcollections. Handle several repos.",
    long_description=readme,
    author="Jesper Ribbe",
    author_email="jesper@ribbe.se",
#    url="https://",
    license=license,
    packages=setuptools.find_packages(exclude=("tests", "docs"))
)



