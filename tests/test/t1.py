#!/usr/bin/python3

import os
import shutil

GIT = "../../git_scm-jesper/git"

def git_cmd(repo_dir:str, cmd:str) -> None:
    old_dir = os.getcwd()
    print (f"*{repo_dir}: git {cmd}")
    try:
        os.chdir(repo_dir)
        os.system(f"{GIT} {cmd}")
    finally:
        os.chdir(old_dir)

def main():
    global GIT

    if not os.path.exists(GIT):
        raise Exception("Cannot find git:",GIT)
    GIT = os.path.abspath(GIT)
    print ("git:", GIT)
    os.system(f"{GIT} --version")

    os.system(f"{GIT} config --global --unset-all submodule.uidHook")
    os.system(f"{GIT} config --global submodule.uidHook \"{os.path.abspath('../gcln.py')} uidhook\"")
    os.system(f"{GIT} config --global -l")

    shutil.rmtree("tmp", ignore_errors=True)
    os.mkdir("tmp")

    git_cmd("tmp", "clone ssh://git@git.ribbe.se:4422/jt/src/src002/005-newmemorycard.git nmc")
    git_cmd("tmp/nmc", "submodule update --init --recursive")

    print ("-"*60)
    print (open("tmp/nmc/.gitmodules","rt").read())
    print (open("tmp/nmc/.git/config","rt").read())
    print ("-"*60)

    os.system("../gcln.py clean_gitmodules")

main()
